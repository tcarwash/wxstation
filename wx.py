import json
import serial
import threading
import time
from os import system, environ, remove, path, stat
from trend import getTrend
from sys import argv
import logging
import yaml


port = environ['WX_PORT']

"""Config file handling"""
config = yaml.safe_load(open("data/wx-config.yml"))
filename = config['global']['data file']
lockfile = config['global']['lock file']
interval = config['wx']['serial interval']
logfile = config['global']['log file']
ageLimit = config['wx']['age limit']

"""Logging"""
logger = logging.getLogger("WX")
logger.setLevel(logging.DEBUG)

fileHandler = logging.FileHandler(logfile)
fileHandler.setLevel(logging.DEBUG)

streamHandler = logging.StreamHandler()
streamHandler.setLevel(logging.WARNING)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fileHandler.setFormatter(formatter)
streamHandler.setFormatter(formatter)

logger.addHandler(fileHandler)
logger.addHandler(streamHandler)

"""The important stuff"""
class getData(threading.Thread):
    def __init__(self, port, filename='./data/data.json', lockfile='./data/data.lock', interval=0, ageLimit=1):
        threading.Thread.__init__(self)
        self.port = port
        self.filename = filename
        self.signal = False
        self.dataLock = lockfile
        self.interval = interval
        self.ageLimit = ageLimit
    def writeCheck(self):
        while path.isfile(self.dataLock):
            print('File in use, waiting')
            time.sleep(5)
        open(self.dataLock, 'a').close()
        return True
    def releaseLock(self):
        remove(self.dataLock)
    def run(self):
        data = []
        counter = 0
        while not self.signal:
            try:
                station = serial.Serial(port, 9600)
                logger.debug('Serial connection opened')
                byte = station.readline()
            except:
                if counter % 10 == 0:
                    if counter == 0:
                        logger.info('Sensor Unavailable')
                    else:
                        logger.info('Sensor Unavailable %s cycles' % counter)
                print('Waiting for Sensor')
                time.sleep(5)
                system('clear')
                counter += 1
                continue
            counter = 0
            item = {}
            string = byte.decode("utf-8")
            string = string.split('\r')[0]
            item["temperature"] = float(string.split(";")[0])
            item["barometer"] = float(string.split(";")[1])
            item["humidity"] = float(string.split(";")[2])
            item["timestamp"] = time.time()
            if self.writeCheck():
                try:
                    with open(self.filename, 'r') as f:
                        data = json.load(f)
                except:
                    print("Creating file")
                    logger.info('Data file does not exist, it will be created')
                    pass
                data.append(item)
                # Throw out data older than limit
                data = [x for x in data if not time.time() - x['timestamp'] > self.ageLimit * 86400]
                with open(self.filename, 'w') as f:
                    json.dump(data, f)
                self.releaseLock()
            station.close()
            logger.debug('Serial connection closed')
            logger.debug('sleeping %s seconds' % interval)
            time.sleep(self.interval)
    def stop(self):
        print("Signal caught! Exiting!")
        self.signal = True
        logger.info('Signal caught, exiting.')

""" Function to allow standalone interactive running of module """
def interactive(filename):
    while True:
        try:
            with open(filename, 'r') as f:
                data = json.load(f)
        except:
            print("Waiting for new data")
            time.sleep(1)
            system("clear")
            continue
        selection = input("h, t, p, c, T, or stop? ")
        if selection == "h":
            print("Humidity: %s%%" % str(round(data[-1]["humidity"], 1)))
        elif selection == "t":
            print("Temperature: %sF" % str(round((data[-1]["temperature"]* 9 / 5 + 32),2)))
        elif selection == "p":
            print("Relative Pressure: %s inHg and " % str(round((data[-1]["barometer"] * 0.02953337),2)) + getTrend(data))
        elif selection == "c":
            system("clear")
        elif selection == "T":
            print("Most recent timestamp: %s" % str(data[-1]['timestamp']))
        elif selection == "stop":
            thread.stop()
            return False

if __name__ == "__main__":
    try:
        if argv[1]:
            if argv[1] == "-h" or argv[1] == "--help":
                print('usage: python3 wx.py options \n  options: \
                \n   -h, --help   shows this page \
                \n   -i, --interactive   shows interactive output \
                \n \n  if run without options, thread is run in background, type "end" to stop')
            elif argv[1] == "-i" or argv[1] == "--interactive":
                thread = getData(port, filename, lockfile, interval)
                thread.start()
                interactive(filename)
            else:
                print('That is not a valid option! Try --help for valid options!')
                pass
    except:
        thread = getData(port, filename, lockfile, interval, ageLimit)
        thread.start()
        stop = input('Thread running! Type "end" to stop thread: ')
        while stop != 'end':
            pass
        thread.stop()
