import unittest
import time

import trend
import wx

class Trend(unittest.TestCase):
    def test_slightUp(self):
        now = time.time()
        data = [{"barometer": 1017, "timestamp": now}, {"barometer": 1018, "timestamp": now}]
        self.assertEqual(trend.getTrend(data), "rising slightly")
    def test_slightDn(self):
        now = time.time()
        data = [{"barometer": 1017, "timestamp": now}, {"barometer": 1016, "timestamp": now}]
        self.assertEqual(trend.getTrend(data), "falling slightly")
    def test_dn(self):
        now = time.time()
        data = [{"barometer": 1017, "timestamp": now}, {"barometer": 1014, "timestamp": now}]
        self.assertEqual(trend.getTrend(data), "falling")
    def test_up(self):
        now = time.time()
        data = [{"barometer": 1017, "timestamp": now}, {"barometer": 1020, "timestamp": now}]
        self.assertEqual(trend.getTrend(data), "rising")

# class Wx(unittest.TestCase):
#    def test_getData(self):
#        data=wx.getData()

if __name__ == "__main__":
    unittest.main()
