/* Arduino sketch for collecting temperature, barometric pressure, and Humidity reporting them via serial.
-- Sketch reports values separated by a semicolin (;)
-- values are reported in order: (Temperature, Pressure, Humidity)

suggestion: add pin to count ticks r/t wind speed, replace delay() with function to
count ticks/time and report.
*/

#include <SFE_BMP180.h>
#include <Wire.h>
#include <DHT.h>

SFE_BMP180 pressure;

#define ALTITUDE 128.93 // Altitude of Station
#define DELAY 5000
#define DHTPIN 2
#define DHTTYPE DHT11

int ledPin = 13;
char status;
double T,P,p0,a;
float h;

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
  if (pressure.begin()) {
  }
  else
  {
    // Error Handling
    Serial.println("BMP180 init fail\n\n");
    while(1); // Pause forever.
  }
  dht.begin();
  delay(2000);
}

void loop() {
  getTemp();
  getPress();
  getHumidity();
  Serial.println(String(T)+";"+String(p0)+";"+String(h));
  delay(DELAY);
}

void getTemp() {
  status = pressure.startTemperature();
  delay(status);
  status = pressure.getTemperature(T);
  delay(status);
}

void getPress() {
  getTemp();
  status = pressure.startPressure(3);
  delay(status);
  status = pressure.getPressure(P,T);
  delay(status);
  p0 = pressure.sealevel(P,ALTITUDE);
}

void getHumidity() {
  if (DELAY < 2000) {
    delay(2000);
  }
  h = dht.readHumidity();
}

void printTemp() {
  Serial.print("Temp: ");
  Serial.print((9.0/5.0)*T+32.0,2);
  Serial.println(" F");
}

void printPress() {
  Serial.print("Rel. Pressure: ");
  Serial.print(P*0.0295333727,2);
  Serial.println(" inHg");
}
