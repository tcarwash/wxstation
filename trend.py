import json
import time
import yaml
from os import path, remove

filename = "data/data.json"
lockfile = "data/data.lock"

def getLock(lockfile):
    while path.isfile(lockfile):
        time.sleep(5)
    open(lockfile, 'a').close()
    return True

def releaseLock(lockfile):
    remove(lockfile)

def openFile(filename, lockfile):
    if getLock(lockfile):
        with open(filename, 'r') as f:
            data = json.load(f)
        releaseLock(lockfile)
        return data

def getTrend(data):
    """Takes a set of json data including timestamps in epoch timestamp
    and barometer data and returns whether barometer is rising or falling over
    length of dataset up to 3 hours"""
    recents = []
    for item in data:
        if (time.time() - item['timestamp']) <= 10800:
            recents.append(item['barometer'])
    if recents[-1]-recents[0] > 1:
        return "rising"
    elif .1 <= recents[-1]-recents[0] <= 1:
        return "rising slightly"
    elif -.1 <= recents[-1]-recents[0] <= .1:
        return "steady"
    elif -1 <= recents[-1]-recents[0] <= -.1:
        return "falling slightly"
    elif recents[-1]-recents[0] < -1:
        return "falling"

if __name__ == "__main__":
    data = openFile(filename, lockfile)
    print(getTrend(data))
