import aprslib
from os import environ, path, remove
import json
from datetime import datetime
from time import sleep, time
import yaml
import logging

callsign = environ['CALLSIGN'] # your callsign associated with APRS
pwd = environ['PASS'] # your APRS pass code
pidfile = "./data/aprs.pid"

"""Config file handling"""
config = yaml.safe_load(open("data/wx-config.yml"))
filename = config['global']['data file']
lat = config['aprs']['lat']
lon = config['aprs']['lon']
ssid = config['aprs']['ssid']
lockfile = config['global']['lock file']
interval = config['aprs']['aprs interval']
logfile = config['global']['log file']

"""Logging"""
logger = logging.getLogger("APRS")
logger.setLevel(logging.DEBUG)
fileHandler = logging.FileHandler(logfile)
fileHandler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fileHandler.setFormatter(formatter)
logger.addHandler(fileHandler)

def checkLock():
    if path.isfile(lockfile):
        return True

""" Assembles the pieces of an APRS packet and returns a complete packet """
def packetGen():
    observation = ""
    while checkLock():
        print("file in use")
        sleep(1)
    with open(filename, 'r') as f:
        data = json.load(f)
    if len(get_obs(data)) > 0:
        observation = get_obs(data)
    coords = lat + "/" + lon + "_"
    utc_time = datetime.now()
    packet = callsign + ssid + ">APRS,TCPIP*:" + "@" + utc_time.strftime("%d%H%M") + "z" + coords + make_aprs_wx(temperature=data[-1]['temperature'] * (9/5)+32, pressure=data[-1]['barometer']*10, humidity=data[-1]['humidity']) \
     + observation
    return packet

""" Sends a completely formed packet to APRS-IS """
def send(packet):
    # a valid passcode for the callsign is required in order to send
    AIS = aprslib.IS(callsign, passwd=pwd, port=14580)
    AIS.connect()
    # send a single status message
    AIS.sendall(packet)
    AIS.close()

def get_obs(data):
    observations = []
    obs = ""
    for item in data:
        if (time() - item['timestamp']) <= 3600:   #look at the last 1 hour for observations
            try:
                observations.append(item['observation'])
            except:
                pass
    try:
        obs = observations[-1]
    except:
        obs = ""
    return obs

""" Formats the weather portion of the APRS packet """
def make_aprs_wx(wind_dir=None, wind_speed=None, wind_gust=None, temperature=None, rain_last_hr=None, rain_last_24_hrs=None, rain_since_midnight=None, humidity=None, pressure=None):
    # Assemble the weather data of the APRS packet
    def str_or_dots(number, length):
        # If parameter is None, fill with zero, otherwise pad with zero
        if number is None:
            return '.'*length
        else:
            format_type = {
                'int': 'd',
                'float': '.0f',
            }[type(number).__name__]
            return ''.join(('%0',str(length),format_type)) % number
    return "%s/%sg%st%sr%sp%sP%sh%sb%s" % (
        str_or_dots(wind_dir, 3),
        str_or_dots(wind_speed, 3),
        str_or_dots(wind_gust, 3),
        str_or_dots(temperature, 3),
        str_or_dots(rain_last_hr, 3),
        str_or_dots(rain_last_24_hrs, 3),
        str_or_dots(rain_since_midnight, 3),
        str_or_dots(humidity, 2),
        str_or_dots(pressure, 5),
    )

if __name__ == "__main__":
    try:
        open(pidfile, 'a').close()
        while True:
            try:
                send(packetGen())
                packetSent = True
                logger.info('APRS packet sent')
            except:
                print('Packet NOT sent')
                packetSent = False
                logger.info('APRS packet NOT sent')
            if packetSent:
                with open("data/aprs.pid", "w") as f:
                    f.write(str(datetime.now()))
                logger.debug('opened pid file')
                print( str(datetime.now()) + " -- packet sent, waiting: " + str(interval) + " minute(s)")
            sleep(interval * 60)
    except:
        print('aprs could not start or was terminated by the user')
        remove(pidfile)
        logger.debug('pid file removed')
